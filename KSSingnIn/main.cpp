#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQuick>
#include <AppController.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    AppController *appController = new AppController(&app);
    appController->startApplication();

    return app.exec();
}
