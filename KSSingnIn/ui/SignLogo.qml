import QtQuick 2.0

Rectangle {
    width: 100
    height: 100
    radius: width*0.5
    border.color: "grey"
    border.width: 7
    antialiasing: true

    Image{
        anchors.fill: parent
        source: "/icons/splash_logo"
    }

}
