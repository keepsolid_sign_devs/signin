import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Item {
    id: root
    objectName: "root"

    ToolBar {
        id: toolBar
        width: parent.width
        height: 32
        background: Rectangle{
            anchors.fill: parent
            color: "#2980cc"
        }
        Label {
            width: parent.width
            color: "white"
            padding: 10
            text: qsTr("Add contact")
            anchors.verticalCenter: parent.verticalCenter
            font.pointSize: 12
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            id: closeBtn
            objectName: "closeBtn"
            signal closed()
            width: 20
            height: 20
            anchors { right: parent.right; rightMargin: 15; verticalCenter: parent.verticalCenter }
            Image {
                id: closeImg
                width: parent.width
                height: parent.height
                sourceSize.width: width
                sourceSize.height: height
                antialiasing: true
                fillMode: Image.PreserveAspectFit
                source: "http://www.5pointwellness.com/images/closebutton.png"
            }
            // Respond to the signal here.
            onClicked: { closed() }
        }
    }

    ColumnLayout {
        anchors { top: toolBar.bottom; bottom: parent.bottom }
        width: parent.width
        spacing: 10

        Item {
            width: 1
            height: 10
        }


        Item {
            width: 1
            Layout.fillHeight: true
        }

        AbstractButton {
            id: addBtn
            objectName: "addBtn"
            anchors { right: parent.right; rightMargin: 20 }

            background: Rectangle {
                color: addBtn.down? "#2370b3" : addBtn.hovered? "#2e90e6" : "#2980cc"
                implicitWidth: 100
                implicitHeight: 36
            }

            contentItem: Text {
                text: "Add"
                font.pointSize: 12
                font.family: "OpenSans"
                color: "white"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
            }
        }

        Item {
            width: 1
            height: 10
        }
    }
}
