import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

ToolBar {
    id: toolBar
    property string title: "KeepSolid Sign"
    property bool withTrayBtn: true
    property color backgroundColor: "#2980cc"
    property color textColor: "white"
    property Window attachedWindow;

    width: parent.width
    height: 32
    background: Rectangle{
        anchors.fill: parent
        color: backgroundColor
    }

    MouseArea {
        id: titleBarMouseRegion
        property var clickPos
        anchors.fill: parent
        onPressed: {
            clickPos = { x: mouse.x, y: mouse.y }
        }
        onPositionChanged: {
            attachedWindow.x = mousePosition.cursorPos().x - clickPos.x
            attachedWindow.y = mousePosition.cursorPos().y - clickPos.y
        }
    }

    Label {
        width: parent.width
        color: textColor
        padding: 10
        text: title
        anchors.verticalCenter: parent.verticalCenter
        font.pointSize: 12
        horizontalAlignment: Qt.AlignHCenter
    }

    Rectangle{
        id: minimizeBtn
        height: 32
        width: 32
        color: backgroundColor
        anchors.right: closeBtn.left
        Image {
            id: minimizeImg
            visible: withTrayBtn
            anchors.centerIn: minimizeBtn
            sourceSize.width: 12
            sourceSize.height: 12
            source: "/icons/minimize_window"
        }

        MouseArea{
            anchors.fill: parent
            onClicked:  attachedWindow.showMinimized()
        }
    }

    Rectangle{
        id: closeBtn
        height: 32
        width: 32
        color: backgroundColor
        anchors.right: parent.right
        Image {
            id: closeImg
            visible: withTrayBtn
            anchors.centerIn: closeBtn
            sourceSize.width: 12
            sourceSize.height: 12
            source: "/icons/close_window"
        }

        MouseArea{
            anchors.fill: parent
            onClicked:   attachedWindow.close()
        }
    }

}
