import QtQuick 2.0

Rectangle {
    height: icon.height
    width: title.width + icon.width + 8
    Image {
        id: icon
        width: 40
        height: 40
        source: "/icons/ksid_logo"
    }

    Text {
        id: title
        wrapMode: Text.WordWrap
        text: qsTr("Create new KeepSolid ID")
        anchors.leftMargin: 8
        anchors.left: icon.right
        anchors.verticalCenter: icon.verticalCenter
        font.capitalization: Font.AllUppercase
        font.pointSize:  12
        font.family: "OpenSans-Semibold"
        color: "#8493a8"
    }
}
