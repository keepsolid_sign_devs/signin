import QtQuick 2.0

Text {
    id: mainText

    readonly property  color  normalColor: "#2980cc"
    readonly property  color  hoverColor: "#2e90e6"
    readonly property  color  clickedColor: "#2370b3"

    property bool hold: false
    signal btnClicked()

    font.pointSize: 12
    font.family: "OpenSans"

    states: [
        State {
            name: "Normal"
            when: !hold && !mouseArea.containsMouse
            PropertyChanges { target: mainText; color: normalColor}
        },
        State {
            name: "Clicked"
            when: hold
            PropertyChanges { target: mainText; color: clickedColor }
        },

        State {
            name: "Hover"
            when: !hold && mouseArea.containsMouse
            PropertyChanges { target: mainText; color: hoverColor }
        }
    ]

    MouseArea{
        id: mouseArea
        hoverEnabled: true
        anchors.fill: parent
        onPressAndHold: {hold = true; btnClicked()}
        onReleased: hold = false
    }
}
