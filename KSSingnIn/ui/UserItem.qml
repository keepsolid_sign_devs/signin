import QtQuick 2.6
import QtQuick.Layouts 1.1
import com.contacts 1.0

Item {
    readonly property  color  primaryTextColor: "#444444"
    readonly property  color  secondaryTextColor: "#8e8e8e"
    readonly property  color  borderColor: "#ebebeb"

    height: userInfoContainer.height

    property ContactModel userModel

    Image {
        id: userAvatarImage
        source: "/icons/profile_icon"
        width: 36
        height: 36
        anchors.verticalCenter: userInfoContainer.verticalCenter
        anchors.left: parent.left
    }

    ColumnLayout{
        id: userInfoContainer
        anchors.left: userAvatarImage.right
        anchors.leftMargin: 13
        spacing:0

        Text{
            id: nameTextItem
            color: primaryTextColor
            font.family: "OpenSans"
            font.pointSize: 14
            anchors.top: parent.top
            text: userModel.firstName + " " + userModel.lastName
        }

        Text{
            id: emailTextItem
            color: secondaryTextColor
            font.family: "OpenSans"
            font.pointSize: 11
            text: userModel.email
        }

        Text{
            id: companyTextItem
            color: secondaryTextColor
            font.family: "OpenSans"
            font.pointSize: 11
            anchors.top: emailTextItem.bottom
            text: userModel.company
        }
    }



}
