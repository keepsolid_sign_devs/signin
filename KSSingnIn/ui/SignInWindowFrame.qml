import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.1

Rectangle {
    anchors.fill: parent
    property alias footerText: footerText
    property alias footer: signInFooter
    property color footerColor: "#dbdbdb"

    readonly property  color  primaryColor: "#2980cc"
    readonly property  color  borderColor: "#dbdbdb"
    readonly property  color  primaryTextColor: "#444444"
    readonly property  color  accentColor: "#1dbf47"

    Rectangle {
        id: signInFooter
        anchors.bottom: parent.bottom
        width: parent.width
        height: 40
        color: footerColor
        border.color: borderColor

        Text {
            id: footerText
            text:  "By selecting Sign In, you agree to the <a href='http://google.com'><b> <font color='secondaryTextColor'> Terms and Conditions</font> </b></a>"
            onLinkActivated: Qt.openUrlExternally(link)
            width: signInFooter.width
            font.family: "OpenSans"
            anchors.centerIn:  signInFooter
            anchors.rightMargin: 28
            anchors.leftMargin:  28
            horizontalAlignment: Text.AlignHCenter
            font.pointSize:  11
            color: primaryTextColor
            wrapMode: Text.Wrap
        }
    }
}

