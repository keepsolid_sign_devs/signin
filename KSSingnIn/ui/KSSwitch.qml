import QtQuick 2.6
import QtQuick.Controls 2.1

Switch{
    id: control
    anchors.right: parent.right

    readonly property  color  activeColor: "#2980cc"
    readonly property  color  inactiveColor: "#ffffff"
    readonly property  color  activeBorderColor: "#2980cc"
    readonly property  color  inactiveBorderColor: "#444444"
    readonly property  color  activeCircleColor: "#ffffff"
    readonly property  color  inactiveCircleColor: "#444444"

    indicator: Rectangle {
        implicitWidth: 40
        implicitHeight: 18
        x: control.leftPadding
        y: parent.height / 2 - height / 2
        radius: 9
        color: control.checked ? activeColor : inactiveColor
        border.color: control.checked ? activeBorderColor : inactiveBorderColor

        Rectangle {
            property int padding: 5
            x: control.checked ? parent.width - width - padding: padding
            anchors.verticalCenter: parent.verticalCenter
            width: 10
            height: 10
            radius: 5
            color: control.checked ? activeCircleColor : inactiveCircleColor
        }
    }
}
