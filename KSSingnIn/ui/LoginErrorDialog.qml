import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.0

ApplicationWindow {
    property alias title: toolBar.title
    property string message: "Enter text of your error"
    signal closed()

    id: dialogContainer
    objectName: "LoginErrorDialog"
    width: 300
    height: 150
    flags: Qt.Dialog | Qt.FramelessWindowHint
    modality: Qt.ApplicationModal

        KSToolbar {
            id: toolBar
            title: title
            attachedWindow: dialogContainer
            withTrayBtn: false
        }

        ColumnLayout {
            anchors { top: toolBar.bottom; bottom: parent.bottom }
            width: parent.width
            spacing: 10

            Item {
                width: 1
                height: 10
            }

            Text {
                id: messageText
                text: message
                width: parent.width * 0.85
                anchors.margins: 20
                height: 30
                font.family: "OpenSans"
                color: "#5e5e5e"
                font.pointSize: 11
                anchors { horizontalCenter: parent.horizontalCenter }
            }

            KSButton {
                id: okBtn
                text: "OK"
                minimumWidth: 100
                anchors { top: messageText.bottom; topMargin: 20; right: parent.right; rightMargin: 20; bottom:parent.bottom; bottomMargin: 20 }
                onClicked: {
                    dialogContainer.close()
                    closed()
                }
            }
        }
}



