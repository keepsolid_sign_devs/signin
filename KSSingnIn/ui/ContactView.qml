import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import com.contacts 1.0

Item {
    readonly property  color  primaryTextColor: "#444444"
    readonly property  color  secondaryTextColor: "#8e8e8e"

    property ContactModel model
    property alias colorIndication: colorIndicator.visible
    property bool dividerVisible: true

    id: root
    height: 30

    RowLayout {
        width: parent.width
        height: parent.height
        spacing: 10

        Rectangle {
            id: wrapperIcon
            width: 30
            height: 30
            radius: width / 2
            antialiasing: true
            color: "white"
            anchors { verticalCenter: parent.verticalCenter; left: parent.left}

            Image {
                width: model.iconSource? 28 : 30
                height: model.iconSource? 28 : 30
                sourceSize.width: width
                sourceSize.height: height
                antialiasing: true
                fillMode: Image.PreserveAspectFit
                anchors { centerIn: wrapperIcon }
                asynchronous: true
                source: model.iconSource ? model.iconSource : "/icons/profile_icon"
            }
            Rectangle {
                id: colorIndicator
                width: 8
                height: width
                radius: width / 2
                border.color: "white"
                border.width: 1
                color: model.color
                anchors { left: parent.left; bottom: parent.bottom }
            }
        }

        ColumnLayout {
            id: textContainer
            Layout.fillWidth: true
            Layout.fillHeight: true
            anchors { verticalCenter: parent.verticalCenter;}
            spacing: 0

            Text {
                id: wrapperTitle
                Layout.fillHeight: true
                Layout.fillWidth: true
                text: model.firstName + " " + model.lastName
                clip: true
                maximumLineCount: 1
                elide: Text.ElideRight
                wrapMode: Text.WrapAnywhere
                color: primaryTextColor
                font.pointSize: 12
                font.family: "OpenSans"
            }

            Text {
                id: wrapperSubtitle
                text: model.email
                Layout.fillWidth: true
                Layout.fillHeight: true
                color: secondaryTextColor
                maximumLineCount: 1
                elide: Text.ElideRight
                wrapMode: Text.WrapAnywhere
                font.pointSize: 9
                font.family: "OpenSans"
                anchors { top: wrapperTitle.bottom;}
            }
        }
    }
}
