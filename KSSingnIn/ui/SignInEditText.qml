import QtQuick 2.0
import QtQuick.Controls 2.1
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.1

Item {
    id: root
    property alias icon: inputField.fieldIconSource
    property alias hintText: inputField.hintText
    property string inputResult: inputField.text
    property alias warningText: inputField.warningText
    property bool isPassword: false

    signal finishEdit(string textResult)

    height: inputField.height

    Layout.minimumWidth: 240
    Layout.minimumHeight: 32
    Layout.preferredHeight:inputField.height

    states: [
        State {
            name: "Disabled"
            when: inputField.state == "Disabled"
            PropertyChanges { target: inputField; actionBtnVisible: false }
        },
        State {
            name: "Active"
            when: inputField.textEdit.activeFocus
        },
        State {
            name: "Error"
            when: inputField.state == "Error"
            PropertyChanges { target: inputField; actionBtnVisible: false }
        },
        State {
            name: "Placeholder"
            when: inputField.state == "Placeholder"
            PropertyChanges { target: inputField; actionBtnVisible: false }
        }
    ]

    InputField{
        id: inputField
        width: parent.width
        actionBtnVisible: text.length != 0
        actionBtnSource: "/icons/close_icon"
        fieldIconVisible: true
        fieldIconSource: root
        isPassword: root.isPassword
        onActionBtnClicked: {
            inputField.text = ""
        }
    }
}
