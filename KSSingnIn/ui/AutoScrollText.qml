import QtQuick 2.7
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0

Flickable{
    id: textContainer
    property alias contentTextWidth: textContainer.contentWidth
    property alias contentTextHeight: textContainer.contentHeight
    property alias animation: scrollAnimation
    property alias contentText: scrollingText
    property int  maxNotScrolledLines

    height: maxNotScrolledLines ? maxNotScrolledLines * fontMetrics.height : parent.height

    function startAnimation(){
        scrollingText.y = 0
        scrollAnimation.start()
    }

    function stopAnimation(){
        scrollingText.y = 0
        scrollAnimation.stop()
    }

    flickableDirection: Flickable.VerticalFlick
    clip: true
    ScrollBar.vertical:  ScrollBar {
        id: scrollBar
    }

    FontMetrics {
        id: fontMetrics
        font.family: scrollingText.font.family
        font.pointSize: scrollingText.font.pointSize
    }

    Text{
        id: scrollingText
        width: scrollingText.width
        wrapMode: Text.WordWrap
        textFormat: Text.StyledText
        anchors.right: parent.right
        anchors.left: parent.left
        font.family: "OpenSans"
        color: primaryTextColor
        font.pointSize: 12

        NumberAnimation on y{
            id: scrollAnimation
            from: 0
            to: -1*scrollingText.height
            loops: Animation.Infinite
            running: false
            duration: 8000
        }

    }
}
