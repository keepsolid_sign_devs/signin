import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import "../styles" 1.0 as Style

Button {
    id: stateButton

    property Style.ButtonStateStyle stateStyle: Style.ButtonTypes.primary //set this property to choose corresponding style for button*/
    property double disabledOpacity: 0.4

    property int minimumWidth: 140
    property int minimumHeight: 36

    readonly property int contentWidth: contentText.paintedWidth + contentText.leftPadding + contentText.rightPadding

    width: contentWidth < minimumWidth ? minimumWidth : contentWidth
    height: minimumHeight

    states: [
        State {
            name: "Disabled"
            when: !enabled
            PropertyChanges { target: background; color: stateStyle.disabledColorBkg }
            PropertyChanges { target: stateButton; opacity: disabledOpacity }
        },
        State {
            name: "Clicked"
            when: down
            PropertyChanges { target: background; color: stateStyle.clickedColorBkg }
        },
        State {
            name: "Hover"
            when: hovered
            PropertyChanges { target: background; color: stateStyle.hoveredColorBkg }
        }
    ]

    FontMetrics {
        id: fontMetrics
        font.family: contentText.font.family
        font.pointSize: contentText.font.pointSize
    }

    contentItem: Text {
        id: contentText
        text: parent.text
        color: stateStyle.textColor
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
//        elide: Text.ElideRight
        font.family: "OpenSans"
        leftPadding: 18
        rightPadding: 18
    }

    background: Rectangle {
        id: background
        anchors.fill: parent
        color: stateStyle.normalColorBkg
    }

}
