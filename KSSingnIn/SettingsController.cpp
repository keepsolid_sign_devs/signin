#include <QDebug>
#include "SettingsController.h"
#include "ContactModel.h"

SettingsController::SettingsController(QObject *parent) : QObject(parent)
{
    _engine = new QQmlApplicationEngine();
    userModel = new ContactModel("Nata", "Tisha", "tester@tet.com", "KeepSolid", "", QColor::fromRgb(255, 204, 0, 255), 0);
}

SettingsController::~SettingsController()
{
    if(_engine) delete _engine;
    if(aboutDialog) delete aboutDialog;
    if(welcomeDialog) delete welcomeDialog;
    if(timer) delete timer;
    if(userModel) delete userModel;
}

void SettingsController::registerTypes(){
    qmlRegisterType<ContactModel>("com.contacts", 1, 0, "ContactModel");
}

void SettingsController::initConnections()
{
    registerTypes();

    _engine->load(QUrl(QLatin1String("qrc:/screens/MainApplicationWindow.qml")));

    //GENERAL
    QQuickItem *generalSettingsQML = _engine->rootObjects().first()->findChild<QQuickItem*>("GeneralSettingsBlock");

    //from QML
    QObject::connect(generalSettingsQML, SIGNAL(lockOnSleepChecked(bool)),
                     this, SLOT(saveLockOnSleep(bool)));
    QObject::connect(generalSettingsQML, SIGNAL(lockOnScreenSaverActivatedChecked(bool)),
                     this, SLOT(saveLockOnScreenSaverActivated(bool)));
    QObject::connect(generalSettingsQML, SIGNAL(lockWhenUserSwitchChecked(bool)),
                     this, SLOT(saveLockWhenUserSwitch(bool)));
    QObject::connect(generalSettingsQML, SIGNAL(lockAfterIdleChecked(bool, int)),
                     this, SLOT(saveLockAfterIdleChecked(bool,int)));
    QObject::connect(generalSettingsQML, SIGNAL(userHelperChecked(bool)),
                     this, SLOT(saveUserHelper(bool)));

    //from C++
    QObject::connect(this, SIGNAL(setLockOnSleep(QVariant)),
                     generalSettingsQML, SLOT(setLockOnSleepChecked(QVariant)));
    QObject::connect(this, SIGNAL(setLockOnScreenSaverActivated(QVariant)),
                     generalSettingsQML, SLOT(setLockOnScreenSaverActivatedChecked(QVariant)));
    QObject::connect(this, SIGNAL(setLockWhenUserSwitch(QVariant)),
                     generalSettingsQML, SLOT(setLockWhenUserSwitchChecked(QVariant)));
    QObject::connect(this, SIGNAL(setLockAfterIdleChecked(QVariant,QVariant)),
                     generalSettingsQML, SLOT(setLockAfterIdleChecked(QVariant, QVariant)));
    QObject::connect(this, SIGNAL(setUserHelper(QVariant)),
                     generalSettingsQML, SLOT(setUserHelperChecked(QVariant)));

    //ACCOUNT
    QQuickItem *accountSettingsQML = _engine->rootObjects().first()->findChild<QQuickItem*>("AccountSettings");

    //from QML
    QObject::connect(accountSettingsQML, SIGNAL(onUserInfoEdited(QString, QString, QString)),
                     this, SLOT(updateUserInfo(QString,QString,QString)));
    //    QObject::connect(accountSettingsQML, SIGNAL(onLogoutPressed())),
    //                     this, SLOT(//someLogoutSlot));
    QObject::connect(accountSettingsQML, SIGNAL(onChangePasswordPressed()),
                     this, SLOT(changePassword()));

    //from C++
    //TODO connect better
    QObject::connect(this, SIGNAL(showUserInfo(QVariant)),
                     accountSettingsQML, SLOT(setUserInfo(QVariant)));
    QObject::connect(this, SIGNAL(showUserInfo(QVariant)),
                     accountSettingsQML, SLOT(updateUserInfo(QVariant)));

    //Clouds
    QQuickItem *cloudSettingsQML = _engine->rootObjects().first()->findChild<QQuickItem*>("CloudsBlock");

    //from QML
    QObject::connect(cloudSettingsQML, SIGNAL(onDropboxSelected(bool)),
                     this, SLOT(dropboxSelected(bool)));
    QObject::connect(cloudSettingsQML, SIGNAL(onDriveSelected(bool)),
                     this, SLOT(googleDriveSelected(bool)));
    QObject::connect(cloudSettingsQML, SIGNAL(onOneDrivelected(bool)),
                     this, SLOT(oneDriveSelected(bool)));
    QObject::connect(cloudSettingsQML, SIGNAL(clearCachePressed()),
                     this, SLOT(clearCache()));

    //from C++
    QObject::connect(this, SIGNAL(setDropboxEnabled(QVariant)),
                     cloudSettingsQML, SLOT(setDropboxEnabled(QVariant)));
    QObject::connect(this, SIGNAL(setGoogleDriveEnabled(QVariant)),
                     cloudSettingsQML, SLOT(setDriveEnabled(QVariant)));
    QObject::connect(this, SIGNAL(setOneDriveEnabled(QVariant)),
                     cloudSettingsQML, SLOT(setOneDriveEnabled(QVariant)));


    //ABOUT
    aboutDialog = new QQuickView(QUrl("qrc:/screens/AboutScreen.qml"));
    QQuickItem *aboutObj = aboutDialog->rootObject();
    QObject::connect(aboutObj, SIGNAL(appInfoClosed()),
                     this, SLOT(closeAboutScreen()));
    QObject::connect(this, SIGNAL(startAboutScreenAnimation()),
                     aboutObj, SLOT(startAnimation()));
    QObject::connect(this, SIGNAL(showAboutScreen(QVariant)),
                     aboutObj, SLOT(setVersion(QVariant)));

    //WELCOME
    welcomeDialog = new QQuickView(QUrl("qrc:/screens/WelcomeScreen.qml"));
    QQuickItem *welcomeObj = welcomeDialog->rootObject();
    QObject::connect(welcomeObj, SIGNAL(windowClosed()),
                     this, SLOT(closeWelcomeScreen()));
    QObject::connect(this, SIGNAL(showWelcomeScreen(QVariant,QVariant)),
                     welcomeObj, SLOT(setUser(QVariant,QVariant)));
}

void SettingsController::displayInitialState()
{
    emit setLockOnSleep(true);
    emit setLockOnScreenSaverActivated(false);
    emit setLockAfterIdleChecked(true, 10);
    emit setLockWhenUserSwitch(false);
    emit setUserHelper(true);
    QVariant var;
    var.setValue(userModel);
    emit showUserInfo(var);
    emit setDropboxEnabled(true);
    emit setOneDriveEnabled(true);
}

void SettingsController::saveLockOnSleep(bool enabled)
{
    qDebug()<<"saveLockOnSleep "<<enabled;
}

void SettingsController::saveLockOnScreenSaverActivated(bool enabled)
{
    qDebug()<<"saveLockOnScreenSaverActivated "<<enabled;
}

void SettingsController::saveLockWhenUserSwitch(bool enabled)
{
    qDebug()<<"saveLockWhenUserSwitch "<<enabled;
}

void SettingsController::saveLockAfterIdleChecked(bool enabled, int minutes)
{
    qDebug()<<"saveLockAfterIdleChecked "<<minutes;
}

void SettingsController::saveUserHelper(bool enabled)
{
    qDebug()<<"saveUserHelper "<<enabled;
}

void SettingsController::changePassword()
{
    qDebug()<<"changePassword pressed";
}

void SettingsController::updateUserInfo(QString firstName, QString lastName, QString company)
{
    //perform server call
    //after that update info or show error message
    userModel->setFirstName(firstName);
    userModel->setLastName(lastName);
    userModel->setCompany(company);
    emit showUserInfo(getUser(userModel));
}

void SettingsController::dropboxSelected(bool enabled){
    qDebug()<<"dropboxSelected "<< enabled;
}

void SettingsController::googleDriveSelected(bool enabled){
    qDebug()<<"googleDriveSelected "<<enabled;
}

void SettingsController::oneDriveSelected(bool enabled){
    qDebug()<<"oneDriveSelected "<<enabled;
}

void SettingsController::clearCache(){
    qDebug()<<"clearCache ";
    createWelcomeWindow();
//    createAboutScreen("66666");
}

void SettingsController::createAboutScreen(QVariant version){
    aboutDialog->setFlags(Qt::FramelessWindowHint);
    aboutDialog->setModality(Qt::ApplicationModal);
    aboutDialog->show();
    timer = new QTimer(this);
    timer->singleShot(750, this, SLOT(aboutIsShowing()));
    timer->start();
    emit showAboutScreen(version);
}

void SettingsController::aboutIsShowing(){
    emit startAboutScreenAnimation();
}

void SettingsController::closeAboutScreen(){
    aboutDialog->close();
}

void SettingsController::createWelcomeWindow(){
    welcomeDialog->setFlags(Qt::FramelessWindowHint);
    welcomeDialog->setModality(Qt::ApplicationModal);
    welcomeDialog->show();
    emit showWelcomeScreen(getUser(userModel), 1);
}

void SettingsController::closeWelcomeScreen(){
    welcomeDialog->close();
}

QVariant SettingsController::getUser(ContactModel *user){
    QVariant var;
    var.setValue(user);
    return var;
}

