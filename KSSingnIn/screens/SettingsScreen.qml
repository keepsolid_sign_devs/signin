import QtQuick 2.6
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.2
import "../ui" as Ui

Flickable{
    readonly property color dividerColor: "#ebebeb"
    objectName: "SettingsScreen"
    contentWidth: mainBlocksContainer.width; contentHeight: mainBlocksContainer.height

    ColumnLayout {
        id: mainBlocksContainer
        anchors.top: parent.top;
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.rightMargin: 30
        anchors.leftMargin: 20
        anchors.topMargin: 30
        spacing: 20

        GeneralSettingsBlock{
            id: generalSettings
            anchors.left: parent.left
            anchors.right: parent.right
        }

        Rectangle{
            color: dividerColor
            width: generalSettings.width
            height: 2
        }

        AccountSettingsBlock{
            id: accountSettingsBlock
            anchors.left: parent.left
            anchors.right: parent.right
        }

        Rectangle{
            color: dividerColor
            width: generalSettings.width
            height: 2
        }

        CloudsSettingsBlock{
            id: cloudsSettingsBlock
            anchors.left: parent.left
            width: generalSettings.width * 0.33
        }

    }

}


