import QtQuick 2.6
import QtQuick.Layouts 1.1
import "../ui" as Ui
import "../styles" 1.0 as Style
import com.contacts 1.0

ColumnLayout {
    id: accountContainer
    objectName: "AccountSettings"
    readonly property  color  primaryColor: "#2980cc"
    property ContactModel user
    spacing: 12

    signal onUserInfoEdited(string newFirstName, string newLastName, string newCompany);
    signal onLogoutPressed();
    signal onChangePasswordPressed();

    function setUserInfo(userModel){
        user = userModel;
    }

    function updateUserInfo(userModel){
        user = userModel;
        exitEditMode()
    }

    function showEditMode(){
        userInfoContainer.visible = false
        editLayout.visible = true
    }

    function exitEditMode(){
        userFirstName.text = user.firstName;
        userLastName.text = user.lastName;
        userCompany.text = user.company;
        userInfoContainer.visible = true
        editLayout.visible = false
    }

    Text{
        id: accountTitle
        text: qsTr("Account")
        font.pointSize: 18
        font.family: "OpenSans"
        anchors.top: accountContainer.top
    }

    RowLayout{
        id: userInfoContainer
        visible: true
        anchors.right: parent.right
        anchors.left: parent.left

        Ui.UserItem{
            id: userItem
            anchors.left: parent.left
            anchors.right: editAccountButton.left
            anchors.rightMargin: 12
            userModel: user
        }

        Ui.KSButton{
            id: editAccountButton
            anchors.right: logoutBtn.left
            text:"Edit"
            anchors.rightMargin: 20
            onClicked: showEditMode()
        }

        Ui.KSButton{
            id: logoutBtn
            anchors.right: parent.right
            stateStyle: Style.ButtonTypes.warning
            text: "Logout"
            onClicked: onLogoutPressed()
        }

    }

    ColumnLayout{
        id: editLayout
        visible: false
        anchors.right: parent.right
        anchors.left: parent.left
        spacing: 12

        RowLayout{
            id: userFieldsContainer
            anchors.right: parent.right
            anchors.left: parent.left
            spacing: 12

            Ui.InputField
            {
                id: userFirstName
                fieldIconVisible: true
                fieldIconSource: "/icons/email_icon"
                Layout.fillWidth: true
                text: user.firstName
            }

            Ui.InputField
            {
                id: userLastName
                fieldIconVisible: true
                fieldIconSource: "/icons/email_icon"
                Layout.fillWidth: true
                text: user.lastName
            }

            Ui.InputField
            {
                id: userCompany
                fieldIconVisible: true
                fieldIconSource: "/icons/company_icon"
                Layout.fillWidth: true
                text: user.company
            }

        }

        RowLayout{
            anchors.right: parent.right
            anchors.left: parent.left

            Ui.KSLink{
                id: changePassBtn
                anchors.left: parent.left
                text: "Change password"
                onBtnClicked: onChangePasswordPressed()
            }

            Ui.KSButton{
                id: cancelBtn
                anchors.right: saveBtn.left
                text: "Cancel"
                stateStyle: Style.ButtonTypes.secondary
                anchors.rightMargin: 6
                onClicked: exitEditMode()
            }

            Ui.KSButton{
                id: saveBtn
                anchors.right: parent.right
                text: "Save"
                onClicked: onUserInfoEdited(userFirstName.text, userLastName.text, userCompany.text)
            }

        }

    }

}
