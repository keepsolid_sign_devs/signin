import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.0
import QtGraphicalEffects 1.0
import com.contacts 1.0
import "../ui" as Ui

ColumnLayout {
    id: root
    objectName: "WelcomeScreen"

    readonly property  color  primaryColor: "#2980cc"
    readonly property  color  backgroundColor: "#f8f8f8"
    readonly property  color  primaryTextColor: "#4a4a4a"
    readonly property  color  secondaryTextColor: "#707070"

    property ContactModel userModel
    property int annotationsToFill
    property string message: "For one thing they usually step all over the hedges and plants on the side of someone’s house killing them and setting back the vegetation’s gardener countless time and effort. For one thing they usually step all over the hedges and plants on the side of someone’s house killing them and setting back the vegetation’s gardener countless time and effort.For one thing they usually step all over the hedges and plants on the side of someone’s house killing them and setting back the vegetation’s gardener countless time and effort. "

    width: 500
    Layout.preferredWidth: 500

    function setUser(user, annotations){
        userModel = user
        annotationsToFill = annotations
    }

    signal windowClosed();

    Rectangle{
        anchors.fill: parent
        color: backgroundColor
    }

    Button{
        id: closeBtn
        width: 30
        height: 30
        Layout.minimumWidth: 30
        Layout.minimumHeight: 30
        Layout.preferredHeight: height
        Layout.preferredWidth: width
        flat: true
        anchors.top: parent.top
        anchors.right: parent.right
        onClicked: windowClosed()

        Image{
            id: closeImg
            width: 18
            height: 18
            anchors.centerIn: parent
            source: "/icons/close_icon"
        }

        ColorOverlay{
            anchors.fill: closeImg
            source: closeImg
            color: primaryTextColor
        }
    }

    ColumnLayout{
        id: mainContentContainer
        anchors.leftMargin: 20
        anchors.rightMargin: 20
        anchors.topMargin: 16
        anchors.fill: parent

        Text{
            id: mainTitle
            text: qsTr("Welcome message")
            anchors.top: parent.top
            Layout.alignment: Qt.AlignLeft
            font.family: "OpenSans"
            font.weight: Font.DemiBold
            color: primaryTextColor
            font.pointSize: 18
        }

        Ui.ContactView{
            id: userItem
            model: userModel
            Layout.alignment: Qt.AlignLeft
            anchors.topMargin: 20
            colorIndication: false
            anchors.top: mainTitle.bottom
            anchors.left: parent.left
            anchors.right: parent.rigth
            Layout.preferredHeight: 35
        }

        ColumnLayout{
            id: messageContainer
            anchors.left: parent.left
            anchors.right: parent.rigth
            anchors.top: userItem.bottom
            width: parent.width

            Image{
                id: messageBackgroundImage
                source: "/icons/message_bckg"
                anchors.fill: parent
                sourceSize.width: 460
                sourceSize.height: messageText.paintedHeight + 70

                Ui.AutoScrollText{
                    id: messageText
                    anchors.topMargin: 16
                    anchors.rightMargin: 12
                    anchors.leftMargin: 12
                    width: parent.width - 24
                    maxNotScrolledLines: 3
                    contentHeight: contentText.height
                    contentText.wrapMode: Text.WordWrap
                    contentText.textFormat: Text.PlainText
                    contentText.text: message
                    anchors.top: messageBackgroundImage.top
                    anchors.left: messageBackgroundImage.left
                    anchors.right: messageBackgroundImage.rigth
                }


                RowLayout{
                    id: annotationsBlock
                    width: parent.width
                    anchors.bottom: messageBackgroundImage.bottom
                    anchors.left: parent.left
                    anchors.right: parent.rigth
                    anchors.margins: 12

                    Rectangle{
                        id: annotationColorRect
                        width: 50
                        height: 20
                        opacity: 0.35
                        border.color: userModel.color
                        color: userModel.color
                        border.width: 1
                    }

                    Text{
                        anchors.left: annotationColorRect.right
                        anchors.leftMargin: 10
                        anchors.verticalCenter: annotationColorRect.verticalCenter
                        text: "You have " + annotationsToFill + (annotationsToFill > 1 ? " <b>annotations</b>" : " <b>annotation</b>") + " to fill"
                    }
                }
            }
        }

        Item{
            id: welcomeFooter
            width: 460
            anchors.left: mainContentContainer.left
            anchors.right: mainContentContainer.rigth
            anchors.top: messageContainer.bottom
            anchors.topMargin: 15
            Layout.preferredHeight: footerTextContainer.height
            height: footerTextContainer.height

            Rectangle{
                id: footerTextContainer
                anchors.left: parent.left
                anchors.right: prevAnnotationIcon.left
                Layout.preferredHeight: height
                height: footerText.paintedHeight + 22
                color: backgroundColor

                Text{
                    id: footerText
                    text: "<b>Note:</b> Use navigation buttons to switch between fields"
                    Layout.alignment: Qt.AlignLeft
                    font.family: "OpenSans"
                    color: primaryTextColor
                    font.pointSize: 12
                    wrapMode: Text.WordWrap
                    anchors.fill: parent
                }

            }

            Image{
                id:prevAnnotationIcon
                source: "/icons/prev_annotation"
                width: 14
                height: 14
                Layout.preferredHeight: height
                Layout.preferredWidth: width
                anchors.right: nextAnnotationIcon.left
                anchors.rightMargin: 5
            }

            Image{
                id: nextAnnotationIcon
                source: "/icons/next_annotation"
                width: 14
                height: 14
                Layout.preferredHeight: height
                Layout.preferredWidth: width
                anchors.right: welcomeFooter.right
            }

        }
    }
}
