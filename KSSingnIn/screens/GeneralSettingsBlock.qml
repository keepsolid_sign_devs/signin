import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import "../ui" as Ui

ColumnLayout {
    id: generalContainer
    property color textColor: "#444444"
    spacing: 20

    objectName: "GeneralSettingsBlock"

    signal lockOnSleepChecked(bool checked);
    signal lockOnScreenSaverActivatedChecked(bool checked);
    signal lockWhenUserSwitchChecked(bool checked);
    signal lockAfterIdleChecked(bool checked, int minutes);
    signal userHelperChecked(bool checked);

    function setLockOnSleepChecked(checked){
        lockOnSleepCb.checked = checked;
    }

    function setLockOnScreenSaverActivatedChecked(checked){
        lockOnScreenSaverCb.checked = checked;
    }

    function setLockWhenUserSwitchChecked(checked){
        lockOnUserSwitchCb.checked = checked;
    }

    function setLockAfterIdleChecked(checked, minutes){
        lockAfterIdleCb.checked = checked;
        lockMinutesInputField.text = minutes;
    }

    function setUserHelperChecked(checked){
        userHelperCb.checked = checked;
    }

    Text {
        id: generalTitle
        text: qsTr("General")
        font.pointSize: 18
        font.family: "OpenSans"
        anchors.top: generalContainer.top
    }

    ColumnLayout {
        id: checkBoxesContainer
        width: parent.width
        spacing: 15
        anchors.right: generalContainer.right;
        anchors.left: generalContainer.left;
        anchors.topMargin: 25

        Ui.KSCheckBox{
            id: lockOnSleepCb
            text: "Lock on sleep"
            leftPadding: 0
            onCheckedChanged: lockOnSleepChecked(checked)
        }

        Ui.KSCheckBox{
            id: lockOnScreenSaverCb
            text: "Lock when screen saver is activated"
            leftPadding: 0
            onCheckedChanged: lockOnScreenSaverActivatedChecked(checked)
        }

        Ui.KSCheckBox{
            id: lockOnUserSwitchCb
            text: "Lock when fast user switching"
            leftPadding: 0
            onCheckedChanged: lockWhenUserSwitchChecked(checked)
        }

        RowLayout{
            height: lockMinutesInputField.height
            spacing: 6

            Ui.KSCheckBox{
                id: lockAfterIdleCb
                text: "Lock after computer idle for"
                leftPadding: 0
                anchors.top: parent.top
                anchors.left: parent.left
                onCheckedChanged: lockAfterIdleChecked(checked, lockMinutesInputField.text)
            }

            Ui.InputField{
                id: lockMinutesInputField
                width: 43
                height: 26
                text: "1"
                anchors.top: parent.top
                textEdit.validator: IntValidator{bottom: 1; top: 100;}
                onTextChanged: lockAfterIdleChecked(lockAfterIdleCb.checked, lockMinutesInputField.text)
            }

            Text{
                height: lockAfterIdleCb.height
                font.pointSize: 12
                font.family: "OpenSans"
                color: textColor
                text: "minutes"
                verticalAlignment: Text.AlignVCenter
                anchors.verticalCenter: lockAfterIdleCb.verticalCenter
            }
        }

        Ui.KSCheckBox{
            id: userHelperCb
            text: "User helper"
            leftPadding: 0
            onCheckedChanged: userHelperChecked(checked)
        }
    }
}
