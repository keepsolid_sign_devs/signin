import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.0
import QtGraphicalEffects 1.0
import "../ui" as Ui

ColumnLayout {
    id: root
    objectName: "AboutScreen"
    readonly property  color  primaryColor: "#2980cc"
    readonly property  color  primaryTextColor: "#4a4a4a"
    readonly property  color  secondaryTextColor: "#707070"
    readonly property  color  fakeScrollBarColor: "#efefef"

    height: 330
    width: 550

    function setVersion(version){
        aboutSubTitle.text = version;
    }

    function startAnimation(){
        textContainer.startAnimation()
    }

    signal appInfoClosed();

    Button{
        id: closeBtn
        width: 30
        height: 30
        Layout.minimumWidth: 30
        Layout.minimumHeight: 30
        Layout.preferredHeight: height
        Layout.preferredWidth: width
        flat: true
        Layout.alignment: Qt.AlignRight | Qt.AlignTop
        onClicked: appInfoClosed()
        Image{
            id: closeImg
            width: 18
            height: 18
            anchors.centerIn: parent
            source: "/icons/close_icon"
        }

        ColorOverlay{
            anchors.fill: closeImg
            source: closeImg
            color: primaryColor
        }
    }


    Rectangle{
        id: aboutLeftPanel
        width: 100
        height: parent.height
        anchors.left: parent.left
        anchors.top: parent.top
        color: primaryColor

        Image{
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.left: parent.left
            height: width
            anchors.margins: 10
            source: "/icons/main_logo"
        }
    }

    ColumnLayout{
        id: aboutMainContainer
        anchors.left: aboutLeftPanel.right
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        Layout.fillHeight: parent
        height: parent.height
        anchors.margins: 20

        Text{
            id: aboutTitle
            text: qsTr("KeepSolid Sign")
            anchors.top: parent.top
            Layout.alignment: Qt.AlignLeft
            font.family: "OpenSans"
            color: primaryTextColor
            font.pointSize: 20
        }

        Text{
            id: aboutSubTitle
            text: qsTr("Version 1.0.666")
            Layout.alignment: Qt.AlignLeft
            font.family: "OpenSans"
            anchors.top: aboutTitle.bottom
            anchors.topMargin: 2
            color: secondaryTextColor
            font.pointSize: 14
        }

        Rectangle{
            id: fakeScrollBar
            color: fakeScrollBarColor
            width: 5
            anchors.left: parent.left
            anchors.top: scrollingText.top
            anchors.bottom: scrollingText.bottom
        }

        Rectangle{
            id: scrollingText
            anchors.right: parent.right
            anchors.left: fakeScrollBar.right
            anchors.top: aboutSubTitle.bottom
            anchors.bottom: aboutFooter.top
            anchors.topMargin: 20
            anchors.bottomMargin: 20
            anchors.leftMargin: 12

            MouseArea{
                anchors.fill: parent
                onWheel: {
                    if(textContainer.animation.running){
                        textContainer.stopAnimation()
                    }
                }
            }

            Ui.AutoScrollText{
                id: textContainer
                anchors.fill: parent
                contentHeight: contentText.height
                contentWidth: parent.width
                contentText.text: "<p><b>Decision-makers</b></p>
                        Vasiliy Ivanov, Antonina Sheremet

                        <p><b>Developers</b></p>
                        Oleg Fokin, Olexander Kuznietsov, Viktoriia Vyhriian, Nataly Tishaninova, Alexey Cherkashyn, Vitalii Tielieusov

                        <p><b>Designer</b></p>
                        Vitalii Kovalenko, Alexandr Mnogosmyslov, Andrew Kalyuzhin, Vanda Kiriak

                        <p><b>Quality Assurance Engineers</b></p>
                        Illya Kochubey, Nickolay Dukov

                        <p><b>PR & Marketing Efforts</b></p>
                        Vira Maiuk, Julia Holovko

                        <p><b>Content</b></p>
                        Julia Glazunova

                        <p><b>Health Coach</b></p>
                        Igor Lakiza"

                animation.running: false
                animation.onStarted: textContainer.interactive = false
                animation.onStopped: textContainer.interactive = true
            }
        }

        RowLayout{
            id: aboutFooter
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.bottom: parent.bottom

            Text{
                text: qsTr("© 2017 KeepSolid Inc.")
                Layout.alignment: Qt.AlignLeft
                font.family: "OpenSans"
                color: secondaryTextColor
                font.pointSize: 12
            }

            Text{
                text: qsTr("<a href='http://google.com'><font color='secondaryTextColor'>Our website</font></a>")
                onLinkActivated: Qt.openUrlExternally(link)
                font.family: "OpenSans"
                anchors.right: parent.right
                color: secondaryTextColor
                font.pointSize: 12
            }

        }


    }
}
