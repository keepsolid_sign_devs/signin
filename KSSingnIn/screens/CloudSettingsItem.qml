import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import "../ui" as Ui

Item {
    readonly property  color  textColor: "#444444"

    property alias icon: cloudIcon.source
    property alias cloudName: cloudName.text
    property alias enabled: cloudSwitch.checked
    property alias cloudSwitch: cloudSwitch

    height: cloudIcon.height

    Image{
        id: cloudIcon
        width: 34
        height: 34
        Layout.preferredHeight: height
        Layout.preferredWidth: width
        anchors.left: parent.left
        source: "/icons/dropbox"
    }

    Text{
        id: cloudName
        anchors.left: cloudIcon.right
        anchors.right: cloudSwitch.left
        font.pointSize: 12
        font.family: "OpenSans"
        color: textColor
        text: "Dropbox"
        anchors.verticalCenter: cloudIcon.verticalCenter
        anchors.leftMargin: 13
    }

    Ui.KSSwitch{
        id: cloudSwitch
        anchors.right: parent.right
        anchors.leftMargin: 13
    }
}
