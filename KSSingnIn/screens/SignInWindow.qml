import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Material 2.0
import "../ui" as Ui
import "../styles" 1.0 as Style

Item {
    id: mainContainer
    anchors.fill: parent
    objectName: "SignInScreen"

    signal registrationBtnPressed();
    signal loginBtnPressed(string email, string password);
    signal forgotPassBtnPressed(string email);
    signal rememberMeChecked(bool checked);

    function showLoginStart(){
        progressIndicator.visible = true;
    }

    function showLoginSuccess(){
        progressIndicator.visible = false;
    }

    function showLoginError(errorMessage){
        progressIndicator.visible = false;
        var component = Qt.createComponent("../ui/LoginErrorDialog.qml");
        if (component.status === Component.Ready) {
            var dialog = component.createObject(parent,{popupType: 1, "title": "Error!", "message": errorMessage});
            dialog.show();
        }
    }

    function showRemindPassStart(){
        progressIndicator.visible = true;
    }

    function showRemindPassSuccess(){
        progressIndicator.visible = false;
    }

    function showRemindPassFail(errorMessage){
        progressIndicator.visible = false;
    }

    function showEmailFieldWarning(warning){
        emailEt.warningText = warning
    }

    function showPasswordFieldWarning(warning){
        passwordEt.warningText = warning
    }

    Ui.SignLogo {
        id: ksIdImg
        anchors.top: mainContainer.top
        anchors.topMargin: 20
        anchors.horizontalCenter: mainContainer.horizontalCenter
    }

    Text {
        id: signInTitle
        text: qsTr("Sign In with Keepsolid ID")
        anchors.top: ksIdImg.bottom
        anchors.topMargin: 20
        anchors.horizontalCenter: mainContainer.horizontalCenter
        font.family: "OpenSans"
        color: primaryTextColor
        font.pointSize: 13
    }

    ColumnLayout{
        id: inputFieldsContainer
        anchors.top: signInTitle.bottom
        anchors.right: mainContainer.right
        anchors.left: mainContainer.left
        anchors.rightMargin : 65
        anchors.leftMargin:  65
        anchors.topMargin: 18
        spacing: 10

        Ui.SignInEditText{
            Layout.fillWidth: true
            id: emailEt
            anchors.topMargin: 10
            icon: "/icons/email_icon"
            hintText:  "Email"
        }

        Ui.SignInEditText{
            Layout.fillWidth: true
            id: passwordEt
            hintText:  "Password"
            icon: "/icons/password_icon"
            isPassword: true
        }

        Ui.KSCheckBox{
            id: rememberPass
            text: qsTr("Remember password")
            anchors.topMargin: 15
            anchors.horizontalCenter: parent.horizontalCenter
            checked: false
            Keys.onUpPressed: nextItemInFocusChain(false).forceActiveFocus()
            onCheckStateChanged: rememberMeChecked(checked);
        }

    }

    Ui.KSButton{
        id: signInBtn
        text: "Sign In"
        anchors.top: inputFieldsContainer.bottom
        anchors.horizontalCenter: mainContainer.horizontalCenter
        anchors.topMargin: 20
        onClicked: {
            console.log("pressed!")
            loginBtnPressed(emailEt.inputResult, passwordEt.inputResult);
        }
    }


    Text {
        id: forgotPassText
        text: qsTr("Forgot password?")
        anchors.bottom: signUpRow.top
        anchors.bottomMargin: 30
        anchors.horizontalCenter: parent.horizontalCenter
        font.underline: true
        font.family: "OpenSans"
        color: secondaryTextColor
        font.pointSize:  12
        MouseArea{
            anchors.fill: parent
            onClicked: forgotPassBtnPressed("sdsad");
        }
    }

    Row{
        id: signUpRow
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 82
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 7

        Text {
            font.family: "OpenSans"
            color: secondaryTextColor
            text: qsTr("Do not have account?")
            font.pointSize:  12
        }

        Text {
            font.family: "OpenSans"
            color: secondaryTextColor
            id: signUpText
            font.underline: true
            text: qsTr("Sign up!")
            font.bold: true
            font.pointSize:  12
            MouseArea{
                anchors.fill: parent
                onClicked: registrationBtnPressed()
            }
        }
    }

    BusyIndicator{
        id: progressIndicator
        anchors.centerIn: parent
        running: true
        visible: false
    }
}
