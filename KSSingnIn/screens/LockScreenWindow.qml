import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Material 2.0
import "../ui" as Ui
import "../styles" 1.0 as Style

Item {
    id: mainContainer
    anchors.fill: parent
    objectName: "LockScreen"

    readonly property  color  primaryColor: "#2980cc"
    readonly property  color  accentColor: "#22d63a"
    readonly property  color  borderColor: "#dbdbdb"
    readonly property  color  primaryTextColor: "#444444"
    readonly property  color  secondaryTextColor: "#5e5e5e"
    readonly property  color  footerColor: "#f9f9f9"
    readonly property  color  mainBackground: "#ffffff"
    readonly property  color  linkTextColor: "#2a2a2a"

    signal onUnlockPressed(string passsword)
    signal onChangeAccount()

    function showUserEmail(email){
        userEmailText.text = email
    }

    function onUnlockingStarted(){

    }

    function onUnlockSuccess(){

    }

    function onUnlockError(error_msg){

    }

    Ui.SignLogo{
        id: ksidLogo
        color: primaryColor
        anchors.top: parent.top
        anchors.horizontalCenter: mainContainer.horizontalCenter
        anchors.topMargin: 60
    }

    ColumnLayout{
        id: editFieldsContainer
        anchors.right: mainContainer.right
        anchors.left: mainContainer.left
        anchors.rightMargin : 65
        anchors.leftMargin:  65
        anchors.topMargin: 40
        anchors.top: ksidLogo.bottom
        spacing: 18

        Text{
            id: userEmailText
            color: primaryTextColor
            font.family: "OpenSans"
            horizontalAlignment: Text.AlignHCenter
            font.pointSize:  10
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            Layout.fillWidth: true
        }

        Ui.SignInEditText{
            id: passwordEt
            icon: "/icons/password_icon"
            hintText:  "Password"
            isPassword: true
            anchors.horizontalCenter: editFieldsContainer.horizontalCenter
            Layout.fillWidth: true
        }

    }

    Ui.KSButton{
        id: unlockBtn
        text: "Unlock"
        width: 140
        height: 36
        anchors.horizontalCenter: mainContainer.horizontalCenter
        anchors.topMargin: 20
        anchors.top: editFieldsContainer.bottom
        onClicked: onUnlockPressed(passwordEt.inputResult)
    }

    Rectangle {
        id: signInFooter
        width: mainContainer.width
        height: 40
        color: footerColor
        border.color: borderColor
        anchors.bottom: mainContainer.bottom

        Text {
            id: footerText
            text:  "<u>Change account</u>"
            width: signInFooter.width
            font.family: "OpenSans"
            anchors.centerIn:  signInFooter
            anchors.rightMargin: 28
            anchors.leftMargin:  28
            horizontalAlignment: Text.AlignHCenter
            font.pointSize:  8
            color: primaryTextColor
            opacity: 0.7
            wrapMode: Text.Wrap

            MouseArea{
                anchors.fill: parent
                onClicked: onChangeAccount()
            }
        }
    }
}
