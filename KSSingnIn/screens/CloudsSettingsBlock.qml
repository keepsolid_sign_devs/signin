import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import "../ui" as Ui
import "../styles" 1.0 as Style

ColumnLayout {
    id: cloudsContainer
    objectName: "CloudsBlock"
    property color textColor: "#444444"

    signal onDropboxSelected(bool enabled);
    signal onDriveSelected(bool enabled);
    signal onOneDrivelected(bool enabled);
    signal clearCachePressed();

    function setDropboxEnabled(enabled){
        dropboxItem.enabled = enabled
    }

    function setDriveEnabled(enabled){
        driveItem.enabled = enabled
    }

    function setOneDriveEnabled(enabled){
        oneDriveItem.enabled = enabled
    }

    Text {
        id: cloudsTitle
        text: qsTr("Clouds")
        font.pointSize: 18
        font.family: "OpenSans"
        anchors.top: cloudsContainer.top
    }

    ColumnLayout {
        id: cloudItemsContainer
        width: cloudsContainer.width
        anchors.right: cloudsContainer.right;
        anchors.left: cloudsContainer.left;
        anchors.top: cloudsTitle.bottom;
        anchors.topMargin: 25
        spacing: 10

        CloudSettingsItem{
            id: dropboxItem
            Layout.fillWidth: true
            cloudName: "Dropbox"
            icon: "/icons/dropbox"
            cloudSwitch.onCheckedChanged:  onDropboxSelected(enabled)
        }

        CloudSettingsItem{
            id: driveItem
            Layout.fillWidth: true
            cloudName: "Google Drive"
            icon: "/icons/drive"
            cloudSwitch.onCheckedChanged:  onDriveSelected(enabled)
        }

        CloudSettingsItem{
            id: oneDriveItem
            Layout.fillWidth: true
            cloudName: "OneDrive"
            icon: "/icons/one_drive"
            cloudSwitch.onCheckedChanged:  onOneDrivelected(enabled)
        }
    }

    Ui.KSButton{
        id: clearCacheBtn
        text: "Clear application's cache"
        anchors.top: cloudItemsContainer.bottom
        anchors.topMargin: 18
        stateStyle: Style.ButtonTypes.secondary
        onClicked: clearCachePressed()
    }
}
