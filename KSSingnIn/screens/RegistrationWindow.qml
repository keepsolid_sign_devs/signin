import QtQuick 2.0
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import "../ui" as Ui
import "../styles" 1.0 as Style

Item {
    id: mainContainer
    objectName: "RegistrationScreen"

    signal registrationBtnPressed(string email, string password, string confirmPassword,
                                  string firstName, string lastName, string company);
    signal cancelBtnPressed();

    function showRegistrationStart(){
        progressIndicator.visible = true;
    }

    function showRegistrationSuccess(){
        progressIndicator.visible = false;
    }

    function showRegistrationError(errorMessage){
        progressIndicator.visible = false;
    }

    function showEmailFieldWarning(warning){
        emailEt.warningText = warning
    }

    function showPasswordFieldsWarning(passwordWarning, confirmPassWarning){
        passwordEt.warningText = passwordWarning
        passwordConfirmEt.warningText = confirmPassWarning
    }

    function showFirstNameWarning(warning){
        firstNameEt.warningText = warning
    }

    function showLastNameWarning(warning){
        lastNameEt.warningText = warning
    }

    Ui.NewKSIDLogo{
        id: ksidLogo
        anchors.top: mainContainer.top
        anchors.horizontalCenter: mainContainer.horizontalCenter
        anchors.topMargin: 40
    }

    ColumnLayout{
        id: editFieldsContainer
        anchors.top: ksidLogo.bottom
        anchors.right: mainContainer.right
        anchors.left: mainContainer.left
        anchors.rightMargin : 65
        anchors.leftMargin:  65
        anchors.topMargin: 18
        spacing: 10

        Ui.SignInEditText{
            id: emailEt
            icon: "/icons/email_icon"
            hintText:  "Email*"
            Layout.fillWidth: true
        }

        Ui.SignInEditText{
            id: passwordEt
            icon: "/icons/password_icon"
            hintText:  "Password*"
            isPassword: true
            Layout.fillWidth: true
        }

        Ui.SignInEditText{
            id: passwordConfirmEt
            icon: "/icons/password_icon"
            hintText:  "Confirm password*"
            isPassword: true
            Layout.fillWidth: true
        }

        Ui.SignInEditText{
            id: firstNameEt
            icon: "/icons/user_small_icon"
            hintText:  "First name*"
            Layout.fillWidth: true
        }

        Ui.SignInEditText{
            id: lastNameEt
            icon: "/icons/user_small_icon"
            hintText:  "Last name*"
            Layout.fillWidth: true
        }

        Ui.SignInEditText{
            id: companyEt
            icon: "/icons/company_icon"
            hintText:  "Company"
            Layout.fillWidth: true
        }

    }

    Ui.KSButton{
        id: signUpBtn
        text: "Sign Up"
        anchors.horizontalCenter: mainContainer.horizontalCenter
        anchors.topMargin: 20
        anchors.top: editFieldsContainer.bottom
        stateStyle: Style.ButtonTypes.cta
        onClicked: registrationBtnPressed(emailEt.inputResult, passwordEt.inputResult, passwordConfirmEt.inputResult,
                                          firstNameEt.inputResult, lastNameEt.inputResult, companyEt.inputResult)
    }

    Ui.KSLink{
        id: cancelBtn
        text: "Cancel"
        anchors.horizontalCenter: mainContainer.horizontalCenter
        anchors.topMargin: 28
        anchors.top: signUpBtn.bottom
        onBtnClicked: cancelBtnPressed()
    }

    BusyIndicator{
        id: progressIndicator
        anchors.centerIn: parent
        running: true
        visible: false
    }
}
