import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Controls.Styles 1.4
import "../ui" as Ui

ApplicationWindow {
    id: appWindow
    objectName: "LoginAppWindow"
    flags: Qt.ToolTip | Qt.FramelessWindowHint | Qt.WA_TranslucentBackground
    visible: true
    title: qsTr("KS Sign")
    minimumHeight: 560
    minimumWidth: 370

    readonly property  color  primaryColor: "#2980cc"
    readonly property  color  accentColor: "#22d63a"
    readonly property  color  borderColor: "#dbdbdb"
    readonly property  color  primaryTextColor: "#444444"
    readonly property  color  secondaryTextColor: "#8e8e8e"
    readonly property  color  footerColor: "#f9f9f9"
    readonly property  color  mainBackground: "#ffffff"
    readonly property  color  linkTextColor: "#2a2a2a"

    header: Ui.KSToolbar{
        id: signInToolbar
        anchors.top: appWindow.top
        attachedWindow: appWindow
    }

    Loader{
        id: loginContentContainer
        objectName: "loginLoader"
        width: appWindow.width
        anchors.top: signInToolbar.bottom
        anchors.bottom: signInFooter.top
        anchors.fill: parent
        source: "SignInWindow.qml"

        function showRegistrationScreen(){
            signInFooter.visible = true
            loginContentContainer.setSource("RegistrationWindow.qml")
        }

        function showLoginScreen(){
            signInFooter.visible = true
            loginContentContainer.setSource("SignInWindow.qml")
        }

        function showUpdateInfoScreen(){
            signInFooter.visible = true
            loginContentContainer.setSource("UpdateInfoWindow.qml")
        }

        function showLockScreen(){
            signInFooter.visible = false
            loginContentContainer.setSource("LockScreenWindow.qml")
        }
    }

    Rectangle {
        id: signInFooter
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        width: appWindow.width
        height: 40
        color: footerColor
        border.color: borderColor

        Text {
            id: footerText
            text:  "By selecting Sign In, you agree to the <a href='http://google.com'><b> <font color='#444444'> Terms and Conditions</font></b></a>"
            onLinkActivated: Qt.openUrlExternally(link)
            width: signInFooter.width
            font.family: "OpenSans"
            anchors.centerIn:  signInFooter
            anchors.rightMargin: 28
            anchors.leftMargin:  28
            horizontalAlignment: Text.AlignHCenter
            font.pointSize:  8
            opacity: 0.7
            color: primaryTextColor
            wrapMode: Text.Wrap
        }
    }
}
