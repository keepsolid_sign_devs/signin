import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Controls.Styles 1.4
import "../ui" as Ui

ApplicationWindow {
    id: appWindow
    objectName: "MainWindow"
    //    flags: Qt.ToolTip | Qt.FramelessWindowHint | Qt.WA_TranslucentBackground
    visible: true
    title: qsTr("KS Sign")
    minimumHeight: 720
    minimumWidth: 740

    Loader{
        id: loginContentContainer
        objectName: "mainLoader"
        anchors.fill: parent
        source: "SettingsScreen.qml"

        function showSettingsScreen(){
            loginContentContainer.setSource("SettingsScreen.qml")
        }

    }
}
