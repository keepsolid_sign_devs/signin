import QtQuick 2.0
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import "../ui" as Ui
import "../styles" 1.0 as Style

Item {
    id: mainContainer
    anchors.fill: parent
    objectName: "UpdateInfoScreen"

    signal updateInfoBtnPressed(string firstName, string lastName, string company);
    signal cancelBtnPressed();

    function showUpdateInfoStart(){
        progressIndicator.visible = true;
    }

    function showUpdateInfoSuccess(){
        progressIndicator.visible = false;
    }

    function showUpdateInfoError(errorMessage){
        progressIndicator.visible = false;
    }

    function showFirstNameWarning(warning){
        firstNameEt.warningText = warning
    }

    function showLastNameWarning(warning){
        lastNameEt.warningText = warning
    }

    Ui.NewKSIDLogo{
        id: ksidLogo
        anchors.top: mainContainer.top
        anchors.horizontalCenter: mainContainer.horizontalCenter
        anchors.topMargin: 82
    }

    ColumnLayout{
        id: editFieldsContainer
        anchors.horizontalCenter: mainContainer.horizontalCenter
        anchors.topMargin: 40
        anchors.top: ksidLogo.bottom
        anchors.right: mainContainer.right
        anchors.left: mainContainer.left
        anchors.rightMargin : 65
        anchors.leftMargin:  65
        spacing: 10

        Ui.SignInEditText{
            id: firstNameEt
            icon: "/icons/email_icon"
            hintText:  "First name*"
            Layout.fillWidth: true
        }

        Ui.SignInEditText{
            id: lastNameEt
            icon: "/icons/email_icon"
            hintText:  "Last name*"
            Layout.fillWidth: true
        }

        Ui.SignInEditText{
            id: companyEt
            icon: "/icons/email_icon"
            hintText:  "Company"
            Layout.fillWidth: true
        }
    }

    Ui.KSButton{
        id: saveBtn
        text: "Save"
        anchors.horizontalCenter: mainContainer.horizontalCenter
        anchors.topMargin: 20
        anchors.top: editFieldsContainer.bottom
        onClicked: updateInfoBtnPressed(firstNameEt.inputResult, lastNameEt.inputResult, companyEt.inputResult)
    }

    Ui.KSLink{
        id: cancelBtn
        text: "Cancel"
        anchors.horizontalCenter: mainContainer.horizontalCenter
        anchors.topMargin: 28
        anchors.top: saveBtn.bottom
        onBtnClicked: cancelBtnPressed()
    }

    BusyIndicator{
        id: progressIndicator
        anchors.centerIn: parent
        running: true
        visible: false
    }

}
