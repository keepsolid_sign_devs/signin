#ifndef APPCONTROLLER_H
#define APPCONTROLLER_H

#include <QObject>
#include <QQmlApplicationEngine>
#include "SignInController.h"
#include "SettingsController.h"

class AppController : public QObject
{
    Q_OBJECT
public:
    explicit AppController(QObject *parent = nullptr);
    ~AppController();
private:
    SignInController *signController;
    SettingsController *settingsController;

signals:
    void startMainScreen();

public slots:
    void startApplication();
};

#endif // APPCONTROLLER_H
