#ifndef CURSORPOSPROVIDER_H
#define CURSORPOSPROVIDER_H

#include <QObject>
#include <QtQuick>
#include <QPointF>

class CursorPosProvider : public QObject
{
    Q_OBJECT
public:
    explicit CursorPosProvider(QObject *parent = nullptr);
    ~CursorPosProvider();
    Q_INVOKABLE QPointF cursorPos()
    {
        return QCursor::pos();
    }

signals:

public slots:
};

#endif // CURSORPOSPROVIDER_H
