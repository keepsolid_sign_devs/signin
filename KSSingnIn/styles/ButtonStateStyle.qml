import QtQuick 2.7

Item {
    property color disabledColorBkg
    property color clickedColorBkg
    property color hoveredColorBkg
    property color normalColorBkg
    property color textColor
}
