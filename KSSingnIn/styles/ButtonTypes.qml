pragma Singleton
import QtQuick 2.7
import "." as Styles

Item {
    id: buttonTypes
    readonly property ButtonStateStyle primary: Styles.ButtonStateStyle {
        disabledColorBkg: "#2980cc"
        clickedColorBkg: "#2370b3"
        hoveredColorBkg: "#2e90e6"
        normalColorBkg: "#2980cc"
        textColor: "#ffffff"
    }
    readonly property ButtonStateStyle secondary: Styles.ButtonStateStyle {
        disabledColorBkg: "#ebebeb"
        clickedColorBkg: "#dedede"
        hoveredColorBkg: "#f5f5f5"
        normalColorBkg: "#ebebeb"
        textColor: "#444444"
    }
    readonly property ButtonStateStyle cta: Styles.ButtonStateStyle {
        disabledColorBkg: "#22d63a"
        clickedColorBkg: "#1dbf47"
        hoveredColorBkg: "#20d951"
        normalColorBkg: "#1dbf47"
        textColor: "#ffffff"
    }
    readonly property ButtonStateStyle warning: Styles.ButtonStateStyle {
        disabledColorBkg: "#ebebeb"
        clickedColorBkg: "#dedede"
        hoveredColorBkg: "#f5f5f5"
        normalColorBkg: "#ebebeb"
        textColor: "#f02b2b"
    }
}
