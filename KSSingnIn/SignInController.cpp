#include <QQmlApplicationEngine>
#include "SignInController.h"
#include "CursorPosProvider.h"

SignInController::SignInController (QObject *parent) : QObject(parent)
{
    timer = new QTimer(this);
    _engine = new QQmlApplicationEngine();
    _userEmail = "n.tishaninova@keepsolid.com";
}
SignInController::~SignInController()
{
    if(timer) delete timer;
    if(_engine) delete _engine;
}

void SignInController::initMain(){
    _engine->load(QUrl(QLatin1String("qrc:/screens/LoginAppWindow.qml")));
    QQuickItem *loader = _engine->rootObjects().first()->findChild<QQuickItem*>("loginLoader");
    initLoginConnections();
    CursorPosProvider *mousePosProvider = new CursorPosProvider(this);
    _engine->rootContext()->setContextProperty("mousePosition", mousePosProvider);

    //fragments changing
    QObject::connect(this, SIGNAL(showLoginScreen()),
                     loader, SLOT(showLoginScreen()));
    QObject::connect(this, SIGNAL(showRegistrationScreen()),
                     loader, SLOT(showRegistrationScreen()));
    QObject::connect(this, SIGNAL(showUpdateInfoScreen()),
                     loader, SLOT(showUpdateInfoScreen()));
    QObject::connect(this, SIGNAL(showLockScreen()),
                     loader, SLOT(showLockScreen()));
}

QString SignInController::userEmail(){
    return _userEmail;
}

void SignInController::setUserEmail(QString userEmail){
    _userEmail = userEmail;
}

void SignInController::startLogin(QString email, QString password){

    if(email.isEmpty()){
        emit invalidEmail("Empty email!");
        return;
    }

    if(password.isEmpty()){
        emit invalidPassword("Empty password!");
        return;
    }

    emit loginStarted();
    //emulating network response
    timer->singleShot(3000, this, SLOT(finishLogin()));
    timer->start();
}

void SignInController::finishLogin(){
    //    emit loginError("What's going on?");
    emit showUpdateInfoScreen();
    initUpdateInfoConnections();
}

void SignInController::remindPassword(QString email){
    qDebug() << "Password was sent to email" << email;
}

void SignInController::setRememberMe(bool remember){
    //TODO save chosen by user value
}

void SignInController::performRegistration(QString email, QString password, QString confirmPassword, QString firstName, QString lastName, QString company){
    if(email.isEmpty()){
        emit invalidEmail("Empty email!");
        return;
    }

    if(password.isEmpty()){
        emit invalidPasswords("Empty email!", "");
        return;
    }

    if(password.length() < 6){
        emit invalidPasswords("Short password!", "");
        return;
    }

    if(password != confirmPassword){
        emit invalidPasswords("", "Passwords do not match!");
        return;
    }

    emit registrationStarted();
}

void SignInController::updateInfo(QString firstName, QString lastName, QString company){

    if(firstName.isEmpty()){
        emit invalidFirstName("Empty first name!!");
        return;
    }

    if(lastName.isEmpty()){
        emit invalidLastName("Empty last name!");
        return;
    }

    timer->singleShot(3000, this, SLOT(finishUpdatingInfo()));
    timer->start();
    emit updateInfoStarted();
}

void SignInController::finishUpdatingInfo(){
    emit updateInfoSuccess();
    emit showLockScreen();
    initLockScreenConnections("n.tishaninova@keepsolid.com");
}

void SignInController::changeAccount(){
    emit showLoginScreen();
    initLoginConnections();
}

void SignInController::unlock(QString password){
    emit unlockingStarted();
}

void SignInController::onNeedToSignUp(){
    emit showRegistrationScreen();
    initRegistrationConnections();
}

void SignInController::onRegistrationCancelled(){
    emit showLoginScreen();
    initLoginConnections();
}

void SignInController::onUpdateInfoCancelled(){
    emit showLoginScreen();
    initLoginConnections();
}

void SignInController::onNeedUpdateInfo(){
    emit showUpdateInfoScreen();
    initUpdateInfoConnections();
}

void SignInController::initLoginConnections(){
    QQuickItem *loginQML = _engine->rootObjects().first()->findChild<QQuickItem*>("SignInScreen");

    //from QML
    QObject::connect(loginQML, SIGNAL(loginBtnPressed(QString, QString)),
                     this, SLOT(startLogin(QString, QString)));
    QObject::connect(loginQML, SIGNAL(rememberMeChecked(bool)),
                     this, SLOT(setRememberMe(bool)));
    QObject::connect(loginQML, SIGNAL(forgotPassBtnPressed(QString)),
                     this, SLOT(remindPassword(QString)));
    QObject::connect(loginQML, SIGNAL(registrationBtnPressed()),
                     this, SLOT(onNeedToSignUp()));

    //from C++
    QObject::connect(this, SIGNAL(loginStarted()),
                     loginQML, SLOT(showLoginStart()));
    QObject::connect(this, SIGNAL(loginError(QVariant)),
                     loginQML, SLOT(showLoginError(QVariant)));
    QObject::connect(this, SIGNAL(invalidEmail(QVariant)),
                     loginQML, SLOT(showEmailFieldWarning(QVariant)));
    QObject::connect(this, SIGNAL(invalidPassword(QVariant)),
                     loginQML, SLOT(showPasswordFieldWarning(QVariant)));
    QObject::connect(this, SIGNAL(loginSuccess()),
                     loginQML, SLOT(showLoginSuccess()));
    QObject::connect(this, SIGNAL(remindRequestStarted()),
                     loginQML, SLOT(showRemindPassStart()));
    QObject::connect(this, SIGNAL(remindSuccess()),
                     loginQML, SLOT(showRemindPassSuccess()));
    QObject::connect(this, SIGNAL(remindFail(QVariant)),
                     loginQML, SLOT(showRemindPassFail(QVariant)));
}

void SignInController::initRegistrationConnections(){
    QQuickItem *registrationQML = _engine->rootObjects().first()->findChild<QQuickItem*>("RegistrationScreen");

    //from QML
    QObject::connect(registrationQML, SIGNAL(registrationBtnPressed(QString, QString, QString, QString, QString, QString)),
                     this, SLOT(performRegistration(QString,QString,QString,QString,QString,QString)));
    QObject::connect(registrationQML, SIGNAL(cancelBtnPressed()),
                     this, SLOT(onRegistrationCancelled()));

    //from C++
    QObject::connect(this, SIGNAL(registrationStarted()),
                     registrationQML, SLOT(showRegistrationStart()));
    QObject::connect(this, SIGNAL(registrationError(QVariant)),
                     registrationQML, SLOT(showRegistrationError(QVariant)));
    QObject::connect(this, SIGNAL(registrationSuccess()),
                     registrationQML, SLOT(showRegistrationSuccess()));
    QObject::connect(this, SIGNAL(invalidEmail(QVariant)),
                     registrationQML, SLOT(showEmailFieldWarning(QVariant)));
    QObject::connect(this, SIGNAL(invalidPasswords(QVariant, QVariant)),
                     registrationQML, SLOT(showPasswordFieldsWarning(QVariant, QVariant)));
    QObject::connect(this, SIGNAL(invalidFirstName(QVariant)),
                     registrationQML, SLOT(showFirstNameWarning(QVariant)));
    QObject::connect(this, SIGNAL(invalidLastName(QVariant)),
                     registrationQML, SLOT(showLastNameWarning(QVariant)));
}

void SignInController::initUpdateInfoConnections(){

    QQuickItem *updateInfoQML = _engine->rootObjects().first()->findChild<QQuickItem*>("UpdateInfoScreen");

    //from QMLonUnlockPressed
    QObject::connect(updateInfoQML, SIGNAL(updateInfoBtnPressed(QString, QString, QString)),
                     this, SLOT(updateInfo(QString,QString,QString)));
    QObject::connect(updateInfoQML, SIGNAL(cancelBtnPressed()),
                     this, SLOT(onUpdateInfoCancelled()));

    //from C++
    QObject::connect(this, SIGNAL(updateInfoStarted()),
                     updateInfoQML, SLOT(showUpdateInfoStart()));
    QObject::connect(this, SIGNAL(updateInfoError(QVariant)),
                     updateInfoQML, SLOT(showUpdateInfoError(QVariant)));
    QObject::connect(this, SIGNAL(updateInfoSuccess()),
                     updateInfoQML, SLOT(showUpdateInfoSuccess()));
    QObject::connect(this, SIGNAL(invalidFirstName(QVariant)),
                     updateInfoQML, SLOT(showFirstNameWarning(QVariant)));
    QObject::connect(this, SIGNAL(invalidLastName(QVariant)),
                     updateInfoQML, SLOT(showLastNameWarning(QVariant)));
}

void SignInController::initLockScreenConnections(QVariant email){
    QQuickItem *lockScreenUml = _engine->rootObjects().first()->findChild<QQuickItem*>("LockScreen");

    //from QML
    QObject::connect(lockScreenUml, SIGNAL(onUnlockPressed(QString)),
                     this, SLOT(unlock(QString)));
    QObject::connect(lockScreenUml, SIGNAL(onChangeAccount()),
                     this, SLOT(changeAccount()));

    //from C++
    QObject::connect(this, SIGNAL(unlockingStarted()),
                     lockScreenUml, SLOT(onUnlockingStarted()));
    QObject::connect(this, SIGNAL(showUserEmail(QVariant)),
                     lockScreenUml, SLOT(showUserEmail(QVariant)));
    QObject::connect(this, SIGNAL(unlockSuccess()),
                     lockScreenUml, SLOT(onUnlockSuccess()));
    QObject::connect(this, SIGNAL(unlockFail(QVariant)),
                     lockScreenUml, SLOT(onUnlockError(QVariant)));

    emit showUserEmail(email);
}
