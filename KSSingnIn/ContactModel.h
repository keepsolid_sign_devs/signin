#ifndef CONTACTMODEL_H
#define CONTACTMODEL_H

#include <QObject>
#include <QColor>

class ContactModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString firstName READ firstName WRITE setFirstName NOTIFY firstNameChanged)
    Q_PROPERTY(QString lastName READ lastName WRITE setLastName NOTIFY lastNameChanged)
    Q_PROPERTY(QString email READ email WRITE setEmail NOTIFY emailChanged)
    Q_PROPERTY(QString company READ company WRITE setCompany NOTIFY companyChanged)
    Q_PROPERTY(QString iconSource READ iconSource WRITE setIconSource NOTIFY iconSourceChanged)
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(int status READ status WRITE setStatus NOTIFY statusChanged)

public:
    ContactModel(QObject *parent=0);
    ContactModel(const QString &firstName, const QString &lastName, const QString &email, const QString &company, const QString &iconSource, const QColor &color, const int &status, QObject *parent=0);
    ~ContactModel();

    QString firstName() const;
    QString lastName() const;
    QString email() const;
    QString company() const;
    QString iconSource() const;
    QColor color() const;
    int status() const;

    void setFirstName(const QString &firstName);
    void setLastName(const QString &lastName);
    void setEmail(const QString &email);
    void setCompany(const QString &company);
    void setIconSource(const QString &iconSource);
    void setColor(const QColor &color);
    void setStatus(const int &status);

signals:
    void firstNameChanged();
    void lastNameChanged();
    void emailChanged();
    void companyChanged();
    void iconSourceChanged();
    void colorChanged();
    void statusChanged();

private:
    QString m_first_name;
    QString m_last_name;
    QString m_email;
    QString m_company;
    QString m_icon_source;
    QColor m_color;
    int m_status;

};

#endif // CONTACTMODEL_H
