import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Controls.Styles 1.4
import "screens" as Screens
import "ui" as Ui

ApplicationWindow {
    id: appWindow
//    flags: Qt.ToolTip | Qt.FramelessWindowHint | Qt.WA_TranslucentBackground
    visible: true
    width: 370
    height: 560
    title: qsTr("KS Sign")

    header: Ui.KSToolbar{
        MouseArea{
            anchors.fill: parent
            onMouseXChanged: {
                appWindow.x = x
            }
        }
    }

    Screens.RegistrationWindow{
    }
}
