#ifndef SIGNINCONTROLLER_H
#define SIGNINCONTROLLER_H

#include <QObject>
#include <QtQuick>

class SignInController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString userEmail READ userEmail WRITE setUserEmail)
public:
    explicit SignInController(QObject *parent = nullptr);
    ~SignInController();

    void initMain();
    QString userEmail();
    void setUserEmail(QString userEmail);

private:
    QString _userEmail;
    QTimer *timer;
    QQmlApplicationEngine *_engine;

signals:
    void showLoginScreen();
    void loginStarted();
    void loginError(QVariant error_msg);
    void loginSuccess();
    void invalidEmail(QVariant warning_msg);
    void invalidPassword(QVariant warning_msg);
    void remindRequestStarted();
    void remindSuccess();
    void remindFail(QVariant error_msg);
    void registrationBtnPressed();

    void showRegistrationScreen();
    void registrationStarted();
    void registrationError(QVariant error_msg);
    void registrationSuccess();
    void cancelRegistrationBtnPressed();
    void invalidPasswords(QVariant pass_warning_msg, QVariant confirm_pass_warning_msg);
    void invalidFirstName(QVariant warning_msg);
    void invalidLastName(QVariant warning_msg);

    void showUpdateInfoScreen();
    void updateInfoStarted();
    void updateInfoError(QVariant error_msg);
    void updateInfoSuccess();

    void showLockScreen();
    void showUserEmail(QVariant email);
    void unlockingStarted();
    void unlockSuccess();
    void unlockFail(QVariant error_msg);

public slots:
    void onNeedToSignUp();
    void onRegistrationCancelled();
    void onNeedUpdateInfo();
    void onUpdateInfoCancelled();
    void changeAccount();

    void startLogin(QString email, QString password);
    void remindPassword(QString email);
    void setRememberMe(bool remember);
    void performRegistration(QString email, QString password, QString confirmPassword,
                             QString firstName, QString lastName, QString company);
    void updateInfo(QString firstName, QString lastName, QString company);
    void unlock(QString password);

private slots:
    void finishLogin();
    void finishUpdatingInfo();

private:
        void initLoginConnections();
    void initRegistrationConnections();
    void initUpdateInfoConnections();
    void initLockScreenConnections(QVariant email);
};

#endif // SIGNINCONTROLLER_H
