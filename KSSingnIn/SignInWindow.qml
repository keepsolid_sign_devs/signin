import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Dialogs 1.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Material 2.0
import "../ui" as Ui

Rectangle {
    signal onRememberPassChecked(bool checked);
    signal onLoginBtnPressed(string email, string password);
    signal onSignUpPressed();
    signal onRememerPasswordPressed();

    readonly property  color  primaryColor: "#2980cc"
    readonly property  color  borderColor: "#dbdbdb"
    readonly property  color  primaryTextColor: "#444444"
    readonly property  color  secondaryTextColor: "#5e5e5e"
    readonly property  color  footerColor: "#f9f9f9"
    readonly property  color  mainBackground: "#ffffff"
    readonly property  color  linkTextColor: "#2a2a2a"

    Rectangle {
        id: mainContainer
        width: parent.width
        height: parent.height

        Image {
            id: ksIdImg
            anchors.top: mainContainer.top
            anchors.topMargin: 20
            width: 100
            height: 100
            anchors.horizontalCenter: mainContainer.horizontalCenter
            source: "/resources/ksid_logo"
        }

        Text {
            font.family: "OpenSans"
            id: signInTitle
            color: primaryTextColor
            anchors.top: ksIdImg.bottom
            anchors.topMargin: 20
            anchors.horizontalCenter: mainContainer.horizontalCenter
            text: qsTr("Sign In with Keepsolid ID")
            font.pointSize: 13

        }

        Ui.SignInEditText{
            id: emailEt
            icon: "/resources/email_icon"
            hintText:  "Email"
            anchors.horizontalCenter: mainContainer.horizontalCenter
            anchors.top: signInTitle.bottom
            anchors.topMargin: 18
            anchors.rightMargin : 65
            anchors.leftMargin:  65
            textEdit.focus:  true
            Keys.onPressed: {
                if(event.key == Qt.Key_Down){
                    passwordEt.textEdit.focus = true
                }
            }

            onFinishEdit: {
                console.log(textResult)
                passwordEt.textEdit.focus = true
            }
        }

        Ui.SignInEditText{
            id: passwordEt
            hintText:  "Password"
            icon: "/resources/password_icon"
            isPassword: true
            anchors.horizontalCenter: mainContainer.horizontalCenter
            anchors.top: emailEt.bottom
            anchors.topMargin: 10
            anchors.rightMargin : 65
            anchors.leftMargin:  65
            Keys.onUpPressed: {
                emailEt.textEdit.focus = true
            }

            onFinishEdit: {
                console.log(textResult)
                rememberPass.focus = true
            }
        }

        CheckBox {
            id: rememberPass
            text: qsTr("Remember password")
            font.pointSize: 12
            font.family: "OpenSans"
            anchors.horizontalCenter: mainContainer.horizontalCenter
            anchors.top: passwordEt.bottom
            anchors.topMargin: 15
            Keys.onUpPressed: {
                passwordEt.textEdit.focus = true
            }
            onCheckStateChanged: {
                myController.setRememberMe(checked)
            }

            indicator: Rectangle {
                implicitWidth: 16
                implicitHeight: 16
                x: rememberPass.leftPadding
                y: parent.height / 2 - height / 2
                color: borderColor
                border.color: rememberPass.focus ? primaryColor : borderColor
                Rectangle {
                    width: 10
                    height: 10
                    anchors.centerIn: parent
                    color: primaryTextColor
                    visible: rememberPass.checked
                }
            }
        }

        Button {
            id: signInBtn
            anchors.top: rememberPass.bottom
            anchors.horizontalCenter: mainContainer.horizontalCenter
            anchors.topMargin: 20
            text: qsTr("Sign In")
            contentItem: Text {
                text: signInBtn.text
                opacity: enabled ? 1.0 : 0.3
                color: mainBackground
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
                font.family: "OpenSans"
            }

            onClicked:{
                myController.startLogin(emailEt.inputResult, passwordEt.inputResult)
            }

            background: Rectangle {
                implicitWidth: 100
                implicitHeight: 40
                opacity: enabled ? 1 : 0.3
                color: primaryColor
            }
        }

        Text {
            font.family: "OpenSans"
            id: forgotPassText
            color: secondaryTextColor
            anchors.top: signInBtn.bottom
            anchors.topMargin: 54
            text: qsTr("Forgot password?")
            font.underline: true
            anchors.horizontalCenter: mainContainer.horizontalCenter
            font.pointSize:  12
            MouseArea{
                anchors.fill: parent
                onClicked:{
                    myController.remindPassword(emailEt.inputResult)
                }
            }
        }

        Row{
            anchors.topMargin: 30
            anchors.top: forgotPassText.bottom
            anchors.horizontalCenter: mainContainer.horizontalCenter
            id: signUpRow
            spacing: 7
            Text {
                font.family: "OpenSans"
                color: secondaryTextColor
                text: qsTr("Do not have account?")
                font.pointSize:  12
            }

            Text {
                font.family: "OpenSans"
                color: secondaryTextColor
                id: signUpText
                font.underline: true
                text: qsTr("Sign up!")
                font.bold: true
                font.pointSize:  12
            }
        }

        Rectangle {
            id: signInFooter
            anchors.bottom: mainContainer.bottom
            width: mainContainer.width
            height: 40
            color: footerColor
            border.color: borderColor

            Text {
                font.family: "OpenSans"
                width: signInFooter.width
                anchors.centerIn:  signInFooter
                anchors.rightMargin: 28
                anchors.leftMargin:  28
                id: termsText
                text: "By selecting Sign In, you agree to the <a href='http://google.com'><b> <font color='secondaryTextColor'> Terms and Conditions</font> </b></a>"
                onLinkActivated: Qt.openUrlExternally(link)
                horizontalAlignment: Text.AlignHCenter
                font.pointSize:  11
                color: primaryTextColor
                wrapMode: Text.Wrap
            }
        }
    }

}
