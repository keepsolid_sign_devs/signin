#ifndef SETTINGSCONTROLLER_H
#define SETTINGSCONTROLLER_H

#include <QObject>
#include <QtQuick>
#include <ContactModel.h>

class SettingsController : public QObject
{
    Q_OBJECT
public:
    explicit SettingsController(QObject *parent = nullptr);
    ~SettingsController();

    void initConnections();
    void displayInitialState();

private:
    ContactModel *userModel;
    QQmlApplicationEngine *_engine;
    QQuickView *aboutDialog;
    QQuickView *welcomeDialog;
    QTimer *timer;

signals:
    void showUserInfo(QVariant userModel);

    void setLockOnSleep(QVariant enabled);
    void setLockOnScreenSaverActivated(QVariant enabled);
    void setLockWhenUserSwitch(QVariant enabled);
    void setLockAfterIdleChecked(QVariant enabled, QVariant minutes);
    void setUserHelper(QVariant enabled);

    void setDropboxEnabled(QVariant enabled);
    void setGoogleDriveEnabled(QVariant enabled);
    void setOneDriveEnabled(QVariant enabled);

    void startAboutScreenAnimation();
    void showAboutScreen(QVariant);

    void showWelcomeScreen(QVariant, QVariant);

private:
    void registerTypes();
    void createAboutScreen(QVariant);
    void createWelcomeWindow();
    static QVariant getUser(ContactModel *user);

public slots:
    void saveLockOnSleep(bool enabled);
    void saveLockOnScreenSaverActivated(bool enabled);
    void saveLockWhenUserSwitch(bool enabled);
    void saveLockAfterIdleChecked(bool enabled, int minutes);
    void saveUserHelper(bool enabled);

    void updateUserInfo(QString firstName, QString lastName, QString company);
    void changePassword();

    void dropboxSelected(bool enabled);
    void googleDriveSelected(bool enabled);
    void oneDriveSelected(bool enabled);

    void clearCache();

    void closeAboutScreen();
    void aboutIsShowing();

    void closeWelcomeScreen();
};

#endif // SETTINGSCONTROLLER_H
