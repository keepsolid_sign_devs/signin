#include "AppController.h"
#include <SignInController.h>
#include <SettingsController.h>
#include <QtQuick>

AppController::AppController(QObject *parent) : QObject(parent),
    settingsController(nullptr),
    signController(nullptr)
{

}

AppController::~AppController(){
    if(signController) delete signController;
    if(settingsController) delete settingsController;
}

void AppController::startApplication(){
//    settingsController = new SettingsController(this);
//    settingsController->initConnections();
//    settingsController->displayInitialState();

    signController = new SignInController(this);
    signController->initMain();
}
